$(function () {
  $("#today").click(function () {

    $(".links").toggle("slow");
    $(".rechts").toggle("slow");
    //$("#leaugeView").css("display", "grid");

  });

  loadMatchesOfToday();
});

function loadMatchesOfToday() {
  $.ajax({
    method: "GET",
    headers: {
      "X-Auth-Token": "d8b62be1e2124c52a70d6b79bdb7e62a"
    },
    url: "https://api.football-data.org/v2/matches/235936",
    success: function (result) {
      console.log(result);
      var output = "";

      output += "<p><span data-teamId=\"" + result.match.homeTeam.id + "\">" + result.match.homeTeam.name + "</span> : <span data-teamId=\"" + result.match.awayTeam.id + "\">" + result.match.awayTeam.name + "</span></p>";

      $("#today").html(output);
      $("#today span").click(function (event) {
        loadTeamById(event.currentTarget.attributes[0].nodeValue);
        upcomingMatches(event.currentTarget.attributes[0].nodeValue);
      })

    },
    error: function (error) {
      alert("Error:" + error);
    }

  });
}

function loadTeamById(id) {

  $.ajax({
    method: "GET",
    headers: {
      "X-Auth-Token": "d8b62be1e2124c52a70d6b79bdb7e62a"
    },
    url: "https://api.football-data.org/v2/teams/" + id,
    success: function (result) {
      console.log(result);
      $("#name").html(result.name);
      $("#clubColor").html(result.clubColors);
      $("#year").html(result.founded);
      $("#home").html(result.venue);
      $("#web").html(result.website);
      $("#web").attr("href", result.website);

    },
    error: function (error) {
      alert("Error:" + error);
    }

  });
}

function upcomingMatches(id) {

  $.ajax({
    method: "GET",
    headers: {
      "X-Auth-Token": "d8b62be1e2124c52a70d6b79bdb7e62a"
    },
    url: "https://api.football-data.org/v2/teams/" + id + "/matches?status=SCHEDULED",
    success: function (result) {
      console.log(result);

      var output = "<table>";

      result.matches.forEach(element => {
        output += "<tr><td data-teamId=\"" + element.homeTeam.id + "\">" + element.homeTeam.name + "</td> <td>:</td> <td data-teamId=\"" + element.awayTeam.id + "\">" + element.awayTeam.name + "</td></tr>";
      });
      output += "</table>";
      $("#next").html(output);
    },
    error: function (error) {
      alert("Error:" + error);
    }

  });
}

